package br.com.sicredi.metrics.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@ControllerAdvice
public class AdviceController {
	
	
	@ExceptionHandler(FeignException.class)
	public ResponseEntity<ErroDTO> feingErrorAdvice(FeignException e){
		return ResponseEntity.unprocessableEntity().body(new ErroDTO("ISBN inválido"));
	}

	@AllArgsConstructor
	@Getter
	class ErroDTO{
		String message;
	}
}
