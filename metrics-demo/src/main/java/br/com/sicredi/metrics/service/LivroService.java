package br.com.sicredi.metrics.service;

import java.util.List;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.sicredi.metrics.events.LivroCreated;
import br.com.sicredi.metrics.model.Livro;
import br.com.sicredi.metrics.repository.LivroRepository;
import br.com.sicredi.metrics.validator.ValidatorClient;
import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
@Transactional
public class LivroService {
	
	private final LivroRepository livroRepository;
	private final ApplicationEventPublisher publisher;
	private final ValidatorClient validatorClient;

	public Livro salvar(Livro livro) {
		validatorClient.validateIsbn(livro.getIsbn());
		Livro livroGravado = livroRepository.save(livro);
		publisher.publishEvent(new LivroCreated(livroGravado));
		return livroGravado;
	}
	
	public List<Livro> buscaTodos(){
		return livroRepository.findAll();
	}

}
