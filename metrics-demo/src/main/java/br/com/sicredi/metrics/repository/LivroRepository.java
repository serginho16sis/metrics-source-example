package br.com.sicredi.metrics.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import br.com.sicredi.metrics.model.Livro;

public interface LivroRepository extends MongoRepository<Livro, String> {
}
