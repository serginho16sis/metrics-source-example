package br.com.sicredi.metrics.model;

public enum TipoLivro {
	EBOOK,
	FISICO
}
