package br.com.sicredi.metrics.controller;

import java.net.URI;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import br.com.sicredi.metrics.model.Livro;
import br.com.sicredi.metrics.service.LivroService;
import io.micrometer.core.annotation.Timed;
import lombok.AllArgsConstructor;

@RequestMapping("produtos")
@AllArgsConstructor
@RestController
@Timed(value = "books.requests")
public class LivroController {

	private final LivroService livroService;

	@PostMapping
	public ResponseEntity<Livro> cadastraProduto(
			@RequestBody Livro produto, UriComponentsBuilder uriBuilder) {
		var book = livroService.salvar(produto);
		URI uri = uriBuilder.path("/").path(book.getId()).build().toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@GetMapping(produces = "application/json")
	@ResponseStatus(code = HttpStatus.OK)
	public List<Livro> buscaTodos(){
		return livroService.buscaTodos();
	}

}
