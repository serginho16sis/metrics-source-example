package br.com.sicredi.metrics.model;

import java.math.BigDecimal;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Document
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Livro {
	@Id
	private String id;
	private String nome, descricao, isbn;
	private BigDecimal valorUnitario;
	private TipoLivro tipo;
}
