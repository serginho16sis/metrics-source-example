package br.com.sicredi.metrics.listeners;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import br.com.sicredi.metrics.events.LivroCreated;
import io.micrometer.core.instrument.MeterRegistry;
import lombok.AllArgsConstructor;

@Component
@AllArgsConstructor
public class LivroCreatedListener implements ApplicationListener<LivroCreated>{
	
	private final MeterRegistry register;

	@Override
	public void onApplicationEvent(LivroCreated event) {
	    register.counter("registro.livros", "tipo", event.getTipoString())
	    		.increment();
	}

}
