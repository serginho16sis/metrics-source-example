package br.com.sicredi.metrics.events;

import org.springframework.context.ApplicationEvent;

import br.com.sicredi.metrics.model.Livro;

public class LivroCreated extends ApplicationEvent{
	private static final long serialVersionUID = -5990601144095493329L;

	public LivroCreated(Livro source) {
		super(source);
	}
	
	public Livro getLivro() {
		return (Livro) getSource();
	}
	
	public String getTipoString() {
		return getLivro().getTipo().toString();
	}
}
