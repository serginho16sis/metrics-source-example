package br.com.sicredi.metrics.validator;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import br.com.sicredi.metrics.configs.OkHttpConfig;

@FeignClient(value = "validatorClient", url = "${validator.client.url}", configuration = OkHttpConfig.class)
public interface ValidatorClient {
	
	@GetMapping("/validacao/{isbn}")
	void validateIsbn(@PathVariable String isbn);
}
