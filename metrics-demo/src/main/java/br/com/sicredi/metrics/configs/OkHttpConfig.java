package br.com.sicredi.metrics.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.Feign;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.binder.okhttp3.OkHttpMetricsEventListener;
import okhttp3.OkHttpClient;

@Configuration
public class OkHttpConfig {
		    
    @Bean
    public OkHttpClient customFeignBuilder(MeterRegistry registry) {
    	return new OkHttpClient.Builder()
    		    .eventListener(OkHttpMetricsEventListener.builder(registry, "validator.requests")
    		        .build())
    		    .build();
    }
    
    @Bean
    public Feign.Builder getBuilder(OkHttpClient client){
    	return Feign.builder().client(new feign.okhttp.OkHttpClient(client));
    }
}
