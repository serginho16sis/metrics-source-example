package br.com.sicredi.books.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LivroValidatorController{
	
	@GetMapping("/validacao/{isbn}")
	public ResponseEntity validaLivro(@PathVariable String isbn) {
		if(isbn.startsWith("1")) {
			return ResponseEntity.badRequest().build();
		}
		
		return ResponseEntity.ok().build();
	}
}